function BasketController($scope, $http) {
	$scope.basketContents = [];
	
	$scope.rowPrice = function(row) {
		return row.quantity * row.unitprice;
	};
	$scope.addRow = function() {
		$scope.basketContents.push({ quantity: $scope.quantity, unitprice: $scope.unitprice });
	};
	$scope.basketPrice = function() {
		var result = 0;
		$scope.basketContents.forEach(function(row) {
			result += $scope.rowPrice(row);
		});
		return result;
	};
	$scope.sendBasket = function() {
		$http.post('http://localhost:8081/savebasket', $scope.basketContents)
			.success(function() {
				$scope.saved = 1;
			});
	};
}
